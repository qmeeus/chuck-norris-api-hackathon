// Enable sourcemaps
import 'source-map-support/register';

import cli from 'cli';
import { IResolvers } from 'graphql-tools/dist/Interfaces';
import { IAuthor, IJoke } from './schema';

const authors: IAuthor[] = [
  {
    id: '1',
    firstName: 'Carlos',
    lastName: 'Norris',
    fullName: 'Carlos Ray Norris'
  }
];

const jokes: IJoke[] = [
  {
    id: '1',
    value: 'We tried putting Chuck Norris in a database but he escaped the cell every single time.',
    authorId: '1'
  },
  {
    id: '2',
    value: 'Chuck Norris doesn’t use web standards as the web will conform to him.',
    authorId: '1'
  },
  {
    id: '3',
    value: 'Chuck Norris sits at the stand-up.',
    authorId: '1'
  }
];

const resolvers = {
  Query: {
    author(root, args) {
      return authors.find((author) => author.id === args.id);
    },
    jokes() {
      return jokes;
    },
    joke(root, args) {
      return jokes.find((joke) => joke.id === args.id);
    }
  },
  Author: {
    jokes(author: IAuthor) {
      return jokes.filter((j) => j.authorId === author.id);
    }
  },
  Joke: {
    author(joke: IJoke) {
      return authors.find((author) => author.id === joke.authorId);
    }
  }
} as IResolvers;

export default resolvers;
