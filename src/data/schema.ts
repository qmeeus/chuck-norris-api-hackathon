// Enable sourcemaps
import 'source-map-support/register';

import { addMockFunctionsToSchema, makeExecutableSchema } from 'graphql-tools';
// import mocks from './mocks';
import resolvers from './resolvers';

export interface IJoke {
  id: string;
  value: string;
  rating?: number;
  authorId: string;
}

export interface IAuthor {
  id: string;
  firstName: string;
  lastName: string;
  fullName: string;
}

const typeDefs = `
type Query {
  joke(id: ID!): Joke,
  jokes: [Joke],
  author(id: ID!): Author
}

type Author {
  id: String!,
  firstName: String!,
  lastName: String!,
  fullName: String!
  jokes: [Joke]!
}

type Joke {
  id: ID!,
  value: String!,
  rating: Int,
  author: Author
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

// addMockFunctionsToSchema({ schema, mocks });

export default schema;
