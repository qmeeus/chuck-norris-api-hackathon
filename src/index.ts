// Enable sourcemaps
import 'source-map-support/register';

import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import bodyParser from 'body-parser';
import cli from 'cli';
import express from 'express';
import helmet from 'helmet';
import schema from './data/schema';

const GRAPHQL_PORT = process.env.PORT || 3000;

const graphQLServer = express();
graphQLServer.use(helmet());

// Setup CORS, so we can call this API externally from a browser on other domains
graphQLServer.use((request, response, next) => {
  response.header('Access-Control-Allow-Credentials', 'true');
  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  response.header('Access-Control-Allow-Headers', 'Origin, appid, X-Requested-With, X-HTTP-Method-Override, Content-Type, authorization, Accept');
  response.header('Allow', 'POST, GET, OPTIONS');

  // Enable OPTIONS requests
  if (request.method === 'OPTIONS') {
    response.sendStatus(200);
  }
  else {
    next();
  }
});

// Define end-points
graphQLServer.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
graphQLServer.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

graphQLServer.listen(GRAPHQL_PORT, () =>
  cli.info(
    `GraphiQL is now running on http://localhost:${GRAPHQL_PORT}/graphiql`
  )
);
